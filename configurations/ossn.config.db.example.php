<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */


// replace <<host>> with your database host name;
$Ossn->host = '<<host>>';

// replace <<port>> with your database host name;
$Ossn->port = '<<port>>';

// replace <<user>> with your database username;
$Ossn->user = '<<user>>';

// replace <<password>> with your database password;
$Ossn->password = '<<password>>';

// replace <<dbname>> with your database name;
$Ossn->database = '<<dbname>>';