<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$Ossn->libraries = array(
	'actions',
	'input',
	'languages',
	'relations',
	'entities',
	'file',
	'objects',
	'annotations',
	'system',
	'views',
	'plugins',
	'page',
	'menus',
	'css',
	'cache',
	'javascripts',
	'initialize',
	'users',
	'image',
	'admin',
	'themes',
	'components',
	'upgrade',
	'securitytoken',
);
