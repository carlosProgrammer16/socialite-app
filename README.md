Socialite Social Network [5.1 LTS]
======================================


Socialite Social Network (SSN) is a social networking software written in PHP. It allows you to make a social networking website and helps your members build social relationships, with people who share similar professional or personal interests. It is available in 10 international languages.

OSSN is released under the ***Socialite Social Network License (SSN License)***

Kernighan & Ritchie (K&R Variant 1TBS) coding standard is used for the sSSN.

Languages
==========
* English
* German
* French
* Romanian
* Portuguese
* Turkish
* Spanish
* Italian
* Russian
* Dutch
* Hebrew

Front-End Features
===================
* User Registration
* User Login
* Profile 
* Profile Photo
* Profile Cover
* Add/Remove Friends
* Live Chat
* Wall posts
* Photos
* Ads
* Groups
* Tag friends in posts
* User block system
* User poke system
* Ajax Comments
* Ajax Likes
* Ajax Photos in comments
* Group cover photos
* Repostion Profile/Group cover
* Notifications
* Friend Requests
* Chat Bar
* Invite Friends
* Embed Videos
* Smilies
* SitePages (terms, privacy, about)
* Site Search
* Reset Password
* Newsfeed page
* Post Edit
* Comment Edit
* Mobile Friendly
* A photo gallery view.
* Emojii Support

Backend Features
=================

* Admin Dashboard for site overview
* Online users count (male/female) graph
* Total site users count (by months) graph
* Update Notification
* Add User
* Remove User
* Edit User
* Ads Manager
* Site Cache Settings
* Site Basic Settings
* Unvalidated users
* Manually validate unvalidated users
* and much more components settings

Prerequisite
=============

* PHP version any of 5.6, 7.0, 7.1
* MYSQL 5 OR >
* APACHE
* MOD_REWRITE
* PHP Extensions cURL & Mcrypt should be enabled
* PHP GD Extension
* PHP ZIP Extension
* PHP settings allowurlfopen enabled
* PHP JSON Support
* PHP XML Support
* PHP OpenSSL

    In case of your own server:

        * 512MB RAM
        * 10GB Disk , More disk more data can be stored.
        * 1v CPU
        * Should be Ubuntu Server x64 bits.


Directory Permissions
============
SSN must be installed into a directory writable by the webserver, or it will not function properly. For most Linux servers running Apache v2, making the parent directory writable is very simple:

```
chgrp www-data /path/to/ssn_parent_directory
chmod g+w /path/to/ssn_parent_directory
``` 

If you are not running Apache v2 or are using a different type of webserver, you'll need to read your system's documentation or speak to your server administrator before proceeding.

Installation
============
Visit my website (https://fiatex.io) for an inquiry

DEMO
====
Available upon request


UPGRADE
========

Copyright 2014-2019 Socialite Social Network (https://fiatex.io/)
