<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$OssnCom = new OssnComponents;
if ($OssnCom->upload()) {
    ossn_trigger_message(ossn_print('com:installed'), 'success');
    redirect(REF);
} else {
    ossn_trigger_message(ossn_print('com:install:error'), 'error');
    redirect(REF);
}