<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
?>
<div class="ossn-profile-module-item">
    <div class="module-title">
        <?php echo $params['title']; ?>
    </div>
    <div class="contents">
        <?php echo $params['contents']; ?>
    </div>
</div>