<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
echo ossn_view_form('album/add', array(
    'action' => ossn_site_url() . 'action/ossn/album/add',
    'component' => 'OssnPhotos',
), false);