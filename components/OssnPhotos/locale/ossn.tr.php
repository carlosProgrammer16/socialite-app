<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$tr = array(
    'album:name' => 'Albüm Adı',
    'add:album' => 'Albüm Ekle',
    'photo:select' => 'Fotoğraf Seç',
    'no:albums' => 'Albüm yok',
    'no:photos' => 'Fotoğraf yok',
    'back:to:album' => 'Albüme geri dön',
    'photo:albums' => 'Fotoğraf Albümleri',
	
    'photo:deleted:success' => 'Fotoğraf silindi!',
    'photo:delete:error' => 'Fotoğraf silinemedi! Lütfen daha sonra tekrar deneyiniz.',
	
    'photos' => 'Fotoğraflar',
    'back' => 'Geri',
    'add:photos' => 'Fotoğraf ekle',
    'delete:photo' => 'Fotoğrafı Sil',
	
    'covers' => 'Kapak',
    'cover:view' => 'Kapağı Görüntüle',
    'profile:covers' => 'Kapak Fotoğrafı',
	'delete:album' => 'Albümü sil',
	
	'photo:album:deleted' => 'Albüm silindi',
	'photo:album:delete:error' => 'Albüm silinemedi',
	

);
ossn_register_languages('tr', $tr); 
