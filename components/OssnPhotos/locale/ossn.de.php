<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$de = array(
    'album:name' => 'Album Name',
    'add:album' => 'Album hinzufügen',
    'photo:select' => 'Wähle ein Foto aus',
    'no:albums' => 'Keine Alben',
    'no:photos' => 'Keine Fotos',
    'back:to:album' => 'Zurück zum Album',
    'photo:albums' => 'Foto Alben',
	
    'photo:deleted:success' => 'Profil-Foto erfolgreich gelöscht',
    'photo:delete:error' => 'Das Foto kann nicht gelöscht werden',
	
    'photos' => 'Fotos',
    'back' => 'Zurück',
    'add:photos' => 'Fotos hinzufügen',
    'delete:photo' => 'Foto löschen',
	
    'covers' => 'Titelbilder',
    'cover:view' => 'Titelbild Ansicht',
    'profile:covers' => 'Profil Titelbilder',
	'delete:album' => 'Album löschen',
    
	'photo:album:deleted' => 'Fotoalbum erfolgreich gelöscht',
	'photo:album:delete:error' => 'Fotoalbum kann nicht gelöscht werden',	
);
ossn_register_languages('de', $de); 
