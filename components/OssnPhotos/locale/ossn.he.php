<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$he = array(
    'album:name' => 'שם אלבום',
    'add:album' => 'הוספת אלבום',
    'photo:select' => 'בחירת תמונה',
    'no:albums' => 'אין אלבומים',
    'no:photos' => 'אין תמונות',
    'back:to:album' => 'חזרה לאלבום',
    'photo:albums' => 'אלבומי תמונות',
	
    'photo:deleted:success' => 'התמונה נמחקה בהצלחה!',
    'photo:delete:error' => 'לא ניתן למחוק תמונה, נסו שנית מאוחר יותר.',
	
    'photos' => 'תמונות',
    'back' => 'חזרה',
    'add:photos' => 'הוספת תמונות',
    'delete:photo' => 'מחיקת תמונות',
	
    'covers' => 'תמונות כותרת',
    'cover:view' => 'הצגת תמונת כותרת',
    'profile:covers' => 'כותרות לפרופיל',
	'delete:album' => 'מחיקת אלבום',
	
	'photo:album:deleted' => 'אלבום תמונות נמחק בהצלחה',
	'photo:album:delete:error' => 'לא ניתן למחוק אלבום',
);
ossn_register_languages('he', $he); 
