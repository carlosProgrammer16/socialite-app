<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$en = array(
    'album:name' => 'Album Name',
    'add:album' => 'Add Album',
    'photo:select' => 'Select Photo',
    'no:albums' => 'No Albums',
    'no:photos' => 'No Photos',
    'back:to:album' => 'Back to Album',
    'photo:albums' => 'Photo Albums',
	
    'photo:deleted:success' => 'Photo successfully deleted!',
    'photo:delete:error' => 'Cannot delete photo! Please try again later.',
	
    'photos' => 'Photos',
    'back' => 'Back',
    'add:photos' => 'Add photos',
    'delete:photo' => 'Delete photo',
	
    'covers' => 'Covers',
    'cover:view' => 'Cover View',
    'profile:covers' => 'Profile Covers',
	'delete:album' => 'Delete Album',
	
	'photo:album:deleted' => 'Photo album successfully deleted',
	'photo:album:delete:error' => 'Can not delete photo album',
	

);
ossn_register_languages('en', $en); 
