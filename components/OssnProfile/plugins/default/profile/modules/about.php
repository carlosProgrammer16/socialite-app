<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
?>
<div class="ossn-profile-modules-about">
    <div class="ossn-profile-modules-about-item">
        <div class="label">
            Lives in
        </div>
        <div class="metadata">
            Location
        </div>
    </div>

    <div class="ossn-profile-modules-about-item">
        <div class="label">
            Works at
        </div>
        <div class="metadata">
            Company name
        </div>
    </div>

    <div class="ossn-profile-modules-about-item">
        <div class="label">
            Married to
        </div>
        <div class="metadata">
            Boy/Girl name
        </div>
    </div>

</div>

