<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
$he = array(
    'user:messages' => 'הודעות',
    'inbox' => 'דואר נכנס',
    'send' => 'נשלחו',
    'ossn:message:between' => 'הודעות %s',
    'messages' => 'הודאות',
    'message:placeholder' => 'הכנס טקסט כאן',
    'no:messages' => 'אין לך הודעות.',
	'ossnmessages:deleted' => 'ההודעה נמחקה',
);
ossn_register_languages('he', $he); 
