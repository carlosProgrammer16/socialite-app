<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
$en = array(
    'user:messages' => 'Messaggi',
    'inbox' => 'In entrata',
    'send' => 'Inviati',
    'ossn:message:between' => 'Messaggio da %s',
    'messages' => 'Messaggi',
    'message:placeholder' => 'Inserisci qui il testo',
    'no:messages' => 'Non hai messaggi.',
	'ossnmessages:deleted' => 'Messaggio eliminato',
);
ossn_register_languages('it', $en); 
