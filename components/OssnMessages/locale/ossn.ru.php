<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
$ru = array(
    'user:messages' => 'Сообщения',
    'inbox' => 'Входящие',
    'send' => 'Отправить',
    'ossn:message:between' => 'Сообщения %s',
    'messages' => 'Сообщения',
    'message:placeholder' => 'Введите текст здесь',
    'no:messages' => 'Вам никто не пишет',
	'ossnmessages:deleted' => 'Сообщение удалено',
);
ossn_register_languages('ru', $ru); 
