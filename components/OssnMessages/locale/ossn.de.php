<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
$de = array(
    'user:messages' => 'Nachrichten',
    'inbox' => 'Eingangs-Korb',
    'send' => 'Abschicken',
    'ossn:message:between' => 'Nachrichten %s',
    'messages' => 'Nachrichten',
    'message:placeholder' => 'Schreib was ...',
    'no:messages' => 'keine Nachrichten vorhanden',
	'ossnmessages:deleted' => 'Die Nachricht wurde gel�scht',	
);
ossn_register_languages('de', $de); 