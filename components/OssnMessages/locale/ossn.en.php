<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
$en = array(
    'user:messages' => 'Messages',
    'inbox' => 'Inbox',
    'send' => 'Send',
    'ossn:message:between' => 'Messages %s',
    'messages' => 'Messages',
    'message:placeholder' => 'Enter text here',
    'no:messages' => 'You have no message.',
	'ossnmessages:deleted' => 'Message was deleted',
);
ossn_register_languages('en', $en); 
