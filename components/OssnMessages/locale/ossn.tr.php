<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
$tr = array(
    'user:messages' => 'Mesajlar',
    'inbox' => 'Gelen Kutusu',
    'send' => 'Gönderilenler',
    'ossn:message:between' => 'Mesaj %s',
    'messages' => 'Mesajlar',
    'message:placeholder' => 'Metninizi girin',
    'no:messages' => 'Mesajınız yok.',
	'ossnmessages:deleted' => 'İleti silindi',
);
ossn_register_languages('tr', $tr); 
