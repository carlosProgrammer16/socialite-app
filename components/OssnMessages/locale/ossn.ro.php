<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
$ro = array(
    'user:messages' => 'Mesaje',
    'inbox' => 'Cutie postala',
    'send' => 'Trimite',
    'ossn:message:between' => 'Mesaje %s',
    'messages' => 'Mesaje',
    'message:placeholder' => 'Scrie aici',
    'no:messages' => 'Nu ai mesaje noi.',
	'ossnmessages:deleted' => 'Mesajul a fost șters',	
);
ossn_register_languages('ro', $ro); 
