<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
echo '<div class="ossn-chat-none">'.ossn_print('ossn:chat:no:friend:online').'</div>';
echo '<script>Ossn.ChatBoot();</script>';
