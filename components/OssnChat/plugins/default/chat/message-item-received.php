<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
?>
<div class="message-reciever" id="ossn-message-item-<?php echo $params['id'];?>">
    <div class="user-icon">
        <img src="<?php echo $params['reciever']->iconURL()->smaller; ?>"/>
    </div>
    <div class="ossn-chat-text-data">
        <div class="ossn-chat-triangle ossn-chat-triangle-white"></div>
        <div class="text <?php echo $params['class'];?>">
            <div class="inner" title="<?php echo OssnChat::messageTime($params['time']); ?>">
            	<?php if($params['deleted']){ ?>
                	<span><i class="fa fa-times-circle"></i><?php echo ossn_print('ossnmessages:deleted');?></span>
                <?php } else { ?>
	                <span><?php echo ossn_call_hook('chat', 'message:smilify', null, linkify_chat($params['message'])); ?></span>
                <?php } ?>                  
            </div>
        </div>
    </div>
</div>
