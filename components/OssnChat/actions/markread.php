<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
$friend = input('fid');
if (!empty($friend)) {
    ossn_chat()->markViewed($friend, ossn_loggedin_user()->guid);
}