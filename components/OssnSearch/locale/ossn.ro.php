<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$ro = array(
	'ossn:search' => 'Cautare',
	'result:type' => 'REZULTAT',
	'search:result' => 'Rezultatele cautarii %s',
	'ossn:search:topbar:search' => 'Cauta grupuri, prieteni si mai mult.',
	'ossn:search:no:result' => 'Nici un rezultat!',
);
ossn_register_languages('ro', $ro); 
