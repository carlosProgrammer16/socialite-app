<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$en = array(
	'ossn:search' => 'Search',
	'result:type' => 'RESULT TYPE',
	'search:result' => 'Search results for %s',
	'ossn:search:topbar:search' => 'Search groups, friends and more.',
	'ossn:search:no:result' => 'No results found!',
);
ossn_register_languages('en', $en); 
