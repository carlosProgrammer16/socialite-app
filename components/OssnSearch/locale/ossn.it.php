<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$en = array(
	'ossn:search' => 'Cerca',
	'result:type' => 'TIPO DI RICERCA',
	'search:result' => 'Cerca risultati per %s',
	'ossn:search:topbar:search' => 'Cerca gruppi, amici ed altro.',
	'ossn:search:no:result' => 'Nessun risultato trovato!',
);
ossn_register_languages('it', $en); 
