<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$de = array(
        'ossn:search' => 'Suche',
        'result:type' => 'ERGEBNISSE VOM TYP',
        'search:result' => 'Such-Ergebnis für %s',
        'ossn:search:topbar:search' => 'Finde Gruppen, Freunde und mehr',
        'ossn:search:no:result' => 'Keine Treffer',
);
ossn_register_languages('de', $de); 
