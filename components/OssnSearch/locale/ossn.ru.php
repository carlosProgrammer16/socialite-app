<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$ru = array(
	'ossn:search' => 'Поиск',
	'result:type' => 'Тип вывода',
	'search:result' => 'Искать результаты для %s',
	'ossn:search:topbar:search' => 'Найти друзей, группы и т.п.',
	'ossn:search:no:result' => 'Ничего не найдено!',
);
ossn_register_languages('ru', $ru); 
