<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$he = array(
	'ossn:search' => 'חיפוש',
	'result:type' => 'סוג תוצאה',
	'search:result' => 'תוצאות חיפוש עבור: %s',
	'ossn:search:topbar:search' => 'חפש בקבוצות, חברים ועוד.',
	'ossn:search:no:result' => 'לא נמצאו תוצאות!',
);
ossn_register_languages('he', $he); 
