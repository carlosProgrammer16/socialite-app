<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
echo ossn_view_form('privacy', array(
        'component' => 'OssnWall',
        'id' => 'ossn-wall-privacy-container',
    ), false);