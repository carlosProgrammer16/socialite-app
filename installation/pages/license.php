<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */


echo '<div><div class="layout-installation"><h2>' . ossn_installation_print('ossn:check') . '</h2><div style="margin:0 auto; width:900px;"><div style="background: #fff; padding: 20px; border-radius: 3px; border: 2px dashed #eee;">';
echo nl2br(file_get_contents(dirname(dirname(dirname(__FILE__))) . '/LICENSE.txt'));
echo '</div><br />';
echo '<a href="' . ossn_installation_paths()->url . '?page=settings" class="button-blue primary">'.ossn_installation_print('ossn:install:next').'</a>';
echo '</div><br /><br /></div>';
