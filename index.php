<?php
/**
 * Socialite Social Network
 *
 * @package   Socialite Social Network
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright (C) Carlos Pinto
 * @license   Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
define('OSSN_ALLOW_SYSTEM_START', TRUE);
require_once('system/start.php');
//page handler
$handler = input('h');
//page name
$page = input('p');

//check if there is no handler then load index page handler
if (empty($handler)) {
    $handler = 'index';
}
echo ossn_load_page($handler, $page);
