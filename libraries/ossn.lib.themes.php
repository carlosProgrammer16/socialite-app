<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
 
function ossn_themes_init(){
	ossn_register_plugins_by_path(ossn_default_theme() . 'plugins/');
}
ossn_register_callback('ossn', 'init', 'ossn_themes_init');
