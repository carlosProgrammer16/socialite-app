<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
  header('Content-Type: application/javascript; charset=utf-8');
?>
var OssnLocale = <?php echo ossn_load_json_locales(); ?>;
