<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
?>
<div class="row">
       <div class="col-md-6 col-center ossn-page-contents">
    	<?php 
			$contents = ossn_view_form('user/resetpassword', array(
  					 'action' => ossn_site_url('action/resetpassword'),
    				 'class' => 'ossn-reset-login',
			));
			echo ossn_plugin_view('widget/view', array(
						'title' => ossn_print('reset:password'),
						'contents' => $contents,
			));
			?>	       			
       </div>     
</div>	
