<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
?>
<div class="login-page">
    <div class="ossn-message-done ossn-account-registered">
        <?php echo ossn_print('register:ok:message'); ?>
    </div>
</div>