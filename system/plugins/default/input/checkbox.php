<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */

$class = 'ossn-checkbox-input';
if(isset($params['class'])){
	$class = $class . $params['class'];
}
$value = (isset($params['value'])) ? $params['value'] : '';
$vars  = array();

$defaults = array(
	'disabled' => false,
	'class' => $class,
	'type' => 'checkbox',
);

$params = array_merge($defaults, $params);
unset($params['value']);
$attributes = ossn_args($params);
if(!empty($value) && $value == true){
		$vars['checked'] = 'checked';
}
$vars = ossn_args($vars);
echo  "<div class='checkbox-block'><input {$attributes} {$vars} /><span>{$label}</span></div>";
