<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
 $timestamp = time();
 $token = ossn_generate_action_token($timestamp);
 ?>
 <input type="hidden" name="ossn_ts" value="<?php echo $timestamp;?>" />
 <input type="hidden" name="ossn_token" value="<?php echo $token;?>" />