<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
?>
<div class="margin-top-10 user-search">
    <input type="text" name="search_users" placeholder="<?php echo ossn_print('search'); ?>"/>
</div>
<div class="top-controls right">
    <a href="<?php echo ossn_site_url("administrator/adduser"); ?>" class="btn btn-success"><?php echo ossn_print('add'); ?></a>
</div>