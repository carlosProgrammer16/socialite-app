<?php
/**
 * Socialite  Network
 *
 * @author    SSN Core Developer <kkobtk@gmail.com>
 * @copyright (C) Carlos Pinto
 * @license  Socialite Social Network License (SSN LICENSE)  https://www.fiatex.io
 * @link      https://www.fiatex.io
 */
?>
<table class="ossn-container">
    <tr>
        <td class="image-block" style="text-align: center;width:100%;">
            <?php echo $params['media']; ?>
        </td>
        <td class="info-block">
            <div class="close-viewer" onclick="Ossn.ViewerClose();">X</div>
            <?php echo $params['sidebar']; ?>
        </td>
    </tr>
</table>
